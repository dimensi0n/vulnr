#!/usr/bin/env python3

from termcolor import colored
from pyfiglet import Figlet
from argparse import ArgumentParser
from whois import query
import nmap

parser = ArgumentParser()
parser.add_argument("-a", "--address", help="Url or IP you want to scan")
parser.add_argument("--no-whois", default="0", nargs="?",const=1, type=int)
args = parser.parse_args()
address = args.address

def green(text):
    return print(colored(text, "green"))

def blue(text):
    return print(colored(text, "blue"))

def main():
    figlet = Figlet()
    blue(figlet.renderText("Vulnr"))
    
    if address:
        if args.no_whois == 0:
            green("=====================WHOIS====================")
            domain = query(address)
            green("Domain name: "+domain.name)
            green("Domain registrar : "+domain.registrar)
            green("Domain creation date : "+str(domain.creation_date))
            green("Domain expiration date : "+str(domain.expiration_date))
    else:
        print(colored("You have to give an url or an ip to scan like: \"vulnr.py -a www.google.com\""))

if __name__ == "__main__":
    main()
