# Vulnr

Vulnr is a tool which allows you to scan an address to see the informations about the domain, the operating system the server is running on, the services running on the server and if there are vulnerabilities available in the exploitdb Vulnr will warn you about these.

## USAGE:
	vulnr.py -a [address]

	Replace [address] with the domain or the ip you want to scan.
	If you doesn't want the whois section just add the `--no-whois` argument like :

	vulnr.py -a [address] --no-whois
